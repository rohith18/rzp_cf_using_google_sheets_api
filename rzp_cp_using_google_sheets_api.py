import argparse
import json
from httplib2 import Credentials

from oauth2client.service_account import ServiceAccountCredentials
import gspread

from utils import fetching_json_data, settlement_api, get_cname, unix_timestamp_to_date


# Parsing arguments to get the dates
def parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("--start-month", type=int, required=True)
    parser.add_argument("--start-day", type=int, required=True)
    parser.add_argument("--num-days", type=int, required=True)
    parser.add_argument("--check-payin", type=str)
    parser.add_argument("--check-payout", type=str)

    args = parser.parse_args()
    base_time_stamp, month = get_base_time_stamp(args.start_month)
    from_date = base_time_stamp + (args.start_day - 1)*86400
    to_date = from_date + args.num_days*(86400)

    return args, from_date, to_date, month


# Base time stamp gives the unix time stamp for a specific month
def get_base_time_stamp(start_month):
    month =""
    if start_month == 1:
        month = "Jan"
        base_time_stamp = 1640995200 # January 1st
    elif start_month == 2:
        month = "Feb"
        base_time_stamp = 1643673600 # February 1st
    elif start_month == 3:
        month = "Mar"
        base_time_stamp = 1646092800 # March 1st
    elif start_month == 4:
        month = "Apr"
        base_time_stamp = 1648771200 # April 1st
    else:
        pass

    return base_time_stamp, month


# Fetching json data of razor payin, cashfree payin and razor payout
def fetch_json_data(args, from_date, to_date):

    CUST_TO_ONEP_RZP_GET_URL = "https://api.pharmacyone.io/prod/rzp_transaction"
    CUST_TO_ONEP_CF_POST_URL = "https://api.pharmacyone.io/prod/cf_payments"
    ONEP_TO_RET_RZP_GET_URL = "https://api.pharmacyone.io/prod/rzp_payout"

    ONEP_TO_RET_RZP_GET_HEADERS, CUST_TO_ONEP_RZP_GET_HEADERS = {"session-token": "wantednote"}, {"session-token": "wantednote"}
    CUST_TO_ONEP_CF_POST_HEADERS = {"session-token": "wantednote", "Content-Type": "application/json"}

    CUST_TO_ONEP_CF_BODY = json.dumps({"startDate": from_date, "endDate": to_date})
    CUST_TO_ONEP_RZP_BODY, ONEP_TO_RET_RZP_BODY = None, None

    cust_to_onep_rzp_list = fetching_json_data(CUST_TO_ONEP_RZP_GET_URL, CUST_TO_ONEP_RZP_GET_HEADERS, CUST_TO_ONEP_RZP_BODY, from_date, to_date)
    cust_to_onep_cf_list = fetching_json_data(CUST_TO_ONEP_CF_POST_URL, CUST_TO_ONEP_CF_POST_HEADERS, CUST_TO_ONEP_CF_BODY, from_date, to_date)
    onep_to_ret_rzp_list = fetching_json_data(ONEP_TO_RET_RZP_GET_URL, ONEP_TO_RET_RZP_GET_HEADERS, ONEP_TO_RET_RZP_BODY, from_date, to_date)

    if args.check_payin == "true":
        onep_to_ret_rzp_list += fetching_json_data(ONEP_TO_RET_RZP_GET_URL, ONEP_TO_RET_RZP_GET_HEADERS, ONEP_TO_RET_RZP_BODY, to_date, to_date+172800)
    elif args.check_payout == "true":
        cust_to_onep_rzp_list += fetching_json_data(CUST_TO_ONEP_RZP_GET_URL, CUST_TO_ONEP_RZP_GET_HEADERS, CUST_TO_ONEP_RZP_BODY, from_date-172800, from_date)
        cust_to_onep_cf_list += fetching_json_data(CUST_TO_ONEP_CF_POST_URL, CUST_TO_ONEP_CF_POST_HEADERS, CUST_TO_ONEP_CF_BODY, from_date-172800, from_date)
    else:
        pass

    return cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list


# Getting settlement lists
def get_settlements_list(onep_to_ret_rzp_list):
    settlements_list = []
    final_settlements_list = []

    for item in onep_to_ret_rzp_list:
        if (item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("type") and item.get("source").get("notes").get("type") == "settlement"):
            settlements_list = settlement_api(item.get("source").get("notes").get("id"))
            for item in settlements_list:
                final_settlements_list.append(item)

    return final_settlements_list


# Getting settlement ids
def get_settlements_info(onep_to_ret_rzp_list):
    settlement_ids = []

    settlements_list = get_settlements_list(onep_to_ret_rzp_list)
    settlement_ids = [item.get("id") for item in settlements_list]
    
    return settlement_ids, settlements_list


# Matching total payins (razor and cashfree) with total payouts (razor and settlement)
def matched_pin_pout(onep_to_ret_rzp_list, settlements_list, total_pin_ids):
    matching_pin_pout_1 = {item.get("source").get("notes").get("id"): [item.get("source").get("notes").get("id"), item.get("source").get("utr"), int(item.get("amount")/ 100), item.get("source").get("notes").get("cid"), item.get("settlementId"), unix_timestamp_to_date(item.get("created_at")), get_cname(item.get("source").get("notes").get("cid"))] for item in  onep_to_ret_rzp_list if(item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("id") and item.get("source").get("notes").get("id") in total_pin_ids)}
    matching_pin_pout_2 = {item.get("id"): [item.get("id"), item.get("utr"), item.get("amount"), item.get("cid"), item.get("settlementId"), unix_timestamp_to_date(item.get("paymentTime")), get_cname(item.get("cid"))] for item in settlements_list if item.get("id") in total_pin_ids}

    matching_pin_pout = {**matching_pin_pout_1, **matching_pin_pout_2}

    return matching_pin_pout


# Getting unmatched payins
def unmatched_pin(cust_to_onep_rzp_list, cust_to_onep_cf_list, total_pout_ids):
    unmatching_pin_1 = {item.get("id"): [item.get("id"), item.get("utr"), item.get("amount"), item.get("notes").get("cid"), unix_timestamp_to_date(item.get("created_at")), get_cname(item.get("notes").get("cid"))] for item in  cust_to_onep_rzp_list if item.get("id") not in total_pout_ids}
    unmatching_pin_2 = {item.get("referenceId"): [item.get("referenceId"), item.get("utr"), float(item.get("amount")), item.get("cid"), item.get("paymentTime"), get_cname(item.get("cid"))] for item in cust_to_onep_cf_list if item.get("id") not in total_pout_ids}

    unmatching_pin = {**unmatching_pin_1, **unmatching_pin_2}

    return unmatching_pin


# Getting unmatched payouts
def unmatched_pout(onep_to_ret_rzp_list, settlements_list, total_pin_ids):
    unmatching_pout_1 = {item.get("source").get("notes").get("id"): [item.get("source").get("notes").get("id"), item.get("source").get("utr"), int(item.get("amount")/ 100), item.get("source").get("notes").get("cid"), item.get("settlementId"), unix_timestamp_to_date(item.get("created_at")), get_cname(item.get("source").get("notes").get("cid"))] for item in  onep_to_ret_rzp_list if(item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("id") and item.get("source").get("notes").get("id") not in total_pin_ids)}
    unmatching_pout_2 = {item.get("id"): [item.get("id"), item.get("utr"), item.get("amount"), item.get("cid"), item.get("settlementId"), unix_timestamp_to_date(item.get("paymentTime")), get_cname(item.get("cid"))] for item in settlements_list if item.get("id") not in total_pin_ids}

    unmatching_pout = {**unmatching_pout_1, **unmatching_pout_2}

    return unmatching_pout


# Getting Total Transaction Value
def get_total_transaction_value(matching_pin_pout):
    total_transaction_value = 0
    transaction_values_list = []

    transaction_values_list = [float(value[2]) for value in matching_pin_pout.values() if value[2]] # Here value[2] refers to amount in the list
    total_transaction_value = sum(transaction_values_list)

    return total_transaction_value


# Writing to excel
def write_to_excel(args, month, total_transaction_value, matching_pin_pout, unmatching_pin, unmatching_pout):
    scope = ["https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]

    # After creating a service account, role and a key in google cloud platform, we will get a json file
    credentials = ServiceAccountCredentials.from_json_keyfile_name("service_account.json", scope)
    # Authorizing the account using gspread package
    client = gspread.authorize(credentials)
    workbook = client.open("test_workbook")
    
    write_info(f"from{month}{args.start_day}(nDays-{args.num_days})", matching_pin_pout, workbook)
    write_info(f"from{month}{args.start_day}(nDays-{args.num_days})(unm_pin)", unmatching_pin, workbook)
    write_info(f"from{month}{args.start_day}(nDays-{args.num_days})(unm_pout)", unmatching_pout, workbook)

    write_summary(args, month, total_transaction_value, workbook)
    
    return "Successful"

def write_info(sheet_name, dict_name, workbook):
    sheet = workbook.add_worksheet(sheet_name, rows=10000, cols=10)
    sheet.append_row(["id", "utr", "amount (in Rupees)", "cid", "settlement_id", "created_at", "cname"])

    for value in dict_name.values():
        sheet.append_row(value)


# Writing summary to summary sheet
def write_summary(args, month, total_transaction_value, workbook):
    summary_sheet = workbook.worksheet("summary")
    summary_sheet.append_row([f"from{month}{args.start_day}(NoOfDays-{args.num_days})", total_transaction_value])

# Reading data from excel - test
def test_read(workbook):
    sheet = workbook.worksheet("test_data")
    records = sheet.get_all_records()
    print(records)


def main():
    total_transaction_value = 0

    args, from_date, to_date, month = parse_args()
    cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list = fetch_json_data(args, from_date, to_date)

    # Extracting only "processed" pouts
    onep_to_ret_rzp_list = [item for item in onep_to_ret_rzp_list if (item.get("source") and item.get("source").get("status") == "processed")]

    cust_to_onep_rzp_pin_ids = [item.get("id") for item in cust_to_onep_rzp_list]
    cust_to_onep_cf_pin_ids = [str(item.get("referenceId")) for item in cust_to_onep_cf_list]
    onep_to_ret_rzp_pout_ids = [item.get("source").get("notes").get("id") for item in onep_to_ret_rzp_list if (item.get("source") and item.get("source").get("notes") and item.get("source").get("notes").get("id"))]

    settlement_ids, settlements_list = get_settlements_info(onep_to_ret_rzp_list)

    total_pin_ids = cust_to_onep_rzp_pin_ids + cust_to_onep_cf_pin_ids
    total_pout_ids = onep_to_ret_rzp_pout_ids + settlement_ids

    print("len(total_pin_ids)", len(total_pin_ids))
    print("len(total_pout_ids)", len(total_pout_ids))

    matching_pin_pout = matched_pin_pout(onep_to_ret_rzp_list, settlements_list, total_pin_ids)
    unmatching_pin = unmatched_pin(cust_to_onep_rzp_list, cust_to_onep_cf_list, total_pout_ids)
    unmatching_pout = unmatched_pout(onep_to_ret_rzp_list, settlements_list, total_pin_ids)

    total_transaction_value = get_total_transaction_value(matching_pin_pout)
    print("total_transaction_value", total_transaction_value)

    result = write_to_excel(args, month, total_transaction_value, matching_pin_pout, unmatching_pin, unmatching_pout)


if __name__ == "__main__":
    main()

